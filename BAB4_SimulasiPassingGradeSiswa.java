import java.util.Scanner;

public class BAB4_SimulasiPassingGradeSiswa {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        Integer matematika, indo, inggris, ipa;


        System.out.print("Masukan nilai ujian Matematika:");
        matematika = input.nextInt();
        System.out.print("Masukan nilai ujian Bhs.Indonesia:");
        indo = input.nextInt();
        System.out.print("Masukan nilai ujian Bhs.Inggris:");
        inggris = input.nextInt();
        System.out.print("Masukan nilai ujian IPA:");
        ipa = input.nextInt();

        System.out.println();

        System.out.println("Hasil penerimaan siswa sebagai berikut:");
        System.out.println("=========================================================");

        Boolean syarat79, sastra, informatika;
        Integer total;

        total = (matematika + indo + inggris + ipa) / 4;
        syarat79 = total >= 73;
        System.out.println("Apakah anda memenuhi persyaratan untuk masuk SMK 79:"+ syarat79);

        sastra = indo > 75 && inggris > 75;
        System.out.println("Apakah anda memenuhi persyaratan untuk masuk jurusan sastra:"+ sastra);

        informatika = matematika > 80;
        System.out.println("Apakah anda memenuhi persyaratan untuk masuk jurusan teknik informatika:"+ informatika);
    }
}