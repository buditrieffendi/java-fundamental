import java.util.Scanner;

public class BAB7_TebakAngka {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Integer rahasia, tebak;
        boolean pass = false;

        rahasia = 1 + (int) (Math.random() * 10); 
        System.out.println("I am thinking of number from 1 to 10");
        System.out.println("Can you guess my number?");

        for(int i=0; i<3; i++){
        tebak = input.nextInt();
            if(tebak == rahasia){
                System.out.println("Right! You Won the game");
                pass = true;
                break;
            } else if(Math.abs(tebak-rahasia)==1){
                System.out.println("Hot");
            } else if(Math.abs(tebak-rahasia)==2){
                System.out.println("Warm");
            } else {
                System.out.println("Cold");
            }
        }
        if(pass==false){
            System.out.println("Wrong! You Lost the game: " + rahasia);
        }
    }
}