import java.util.Scanner;

public class BAB7_DiceGame {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Integer lempar, player, computer, total;
        String isLanjut="";

        player = 0;
        computer = 0;

        do{
            if(player<100){
                total = 0;
                System.out.println("Computer point: "+ computer);

                do{
                    lempar = 1+(int)(Math.random()*6);
                    System.out.println("\tComputer rolled a: "+ lempar);
                    if(lempar==1){
                        System.out.println("\tRoll End");
                        total = 0;
                    }else{
                        total += lempar;
                        System.out.println("\tComputer point: "+ total);
                        System.out.println("\tPoint so far this round");
                        if(total<10){
                            System.out.println("\tComputer will roll again");
                        }
                    }
                }while(lempar!=1 && total<10);

                computer += total;
                System.out.println("\tComputer point: "+ computer +".\n");
            }

            total = 0;
            System.out.println("Player point: "+ player);

            do{
                lempar = 1+(int)(Math.random()*6);
                System.out.println("\tPlayer rolled a: "+ lempar);
                if(lempar==1){
                    System.out.println("\tRoll End");
                    total = 0;
                }else{
                    total += lempar;
                    System.out.println("\tYour point: "+ total);
                    System.out.println("\tPoint so far this round");
                    System.out.print("\tWould you like to roll again? (Roll/Hold)");
                    isLanjut = input.next();

                    if(total<10){
                        System.out.println("\tYour will roll again");
                    }
                }
            }while(lempar != 1 && isLanjut.equalsIgnoreCase("Roll"));

            player += total;
            System.out.println("\tYour point: "+ player +".\n");
        }while(player <50 && computer <50);

        if(player>computer){
                System.out.println("Player win");
        }else{
                System.out.println("Computer win");
        }
    }
}