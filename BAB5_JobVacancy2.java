import java.util.Scanner;

public class BAB5_JobVacancy2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String nama, jk, lulusan, reqPosisi, result = "";
        Integer umur, pengalaman;
        Double penampilan;
        Boolean isKoor, koor1, koor2, isAdmin, admin1, admin2, admin3, isSpv, spv1, spv2;

        System.out.print("Nama : ");
        nama = input.nextLine();
        System.out.print("Umur : ");
        umur = input.nextInt();
        System.out.print("Jenis Kelamin(Pria/Wanita) : ");
        jk = input.next();
        System.out.print("Lulusan : ");
        lulusan = input.next();
        System.out.print("Pengalaman (Tahun) : ");
        pengalaman = input.nextInt();
        System.out.print("Penampilan (1/10) : ");
        penampilan = input.nextDouble();
        System.out.println("Posisi yang akan dilamar (Koordinator, Admin, SPV):");
        reqPosisi = input.next();

        if(reqPosisi.equalsIgnoreCase("Koordinator")){
                koor1 = (jk.equalsIgnoreCase("Pria") && umur >= 21 && umur <= 30 && lulusan.equalsIgnoreCase("SMK") || lulusan.equalsIgnoreCase("D3") && pengalaman >= 2);
                koor2 = (jk.equalsIgnoreCase("Pria") && umur >= 30 && lulusan.equalsIgnoreCase("S1") && pengalaman >= 5);

                isKoor = koor1 || koor2;

                if(isKoor){
                        result = "Anda akan masuk ke tahap selanjutnya";
                } else {
                        result = "Anda tidak akan masuk ke tahap selanjutnya";
                }

        }else if(reqPosisi.equalsIgnoreCase("Admin")){
                admin1 = (jk.equalsIgnoreCase("Wanita") && (umur >= 20 && umur <= 25) && lulusan.equalsIgnoreCase("D3") && (penampilan >= 8.5 || pengalaman >= 1));
                admin2 = (jk.equalsIgnoreCase("Wanita") && umur >= 25 && lulusan.equalsIgnoreCase("S1") && penampilan >= 8.5 && pengalaman >= 3);
                admin3 = (jk.equalsIgnoreCase("Pria") && (umur >= 20 && umur <= 30) && penampilan >= 8.5 && (lulusan.equalsIgnoreCase("D3") || lulusan.equalsIgnoreCase("S1")) && pengalaman >= 2);


                isAdmin = admin1 || admin2 || admin3;

                if(isAdmin){
                        result = "Anda akan masuk ke tahap selanjutnya";
                } else {
                        result = "Anda tidak akan masuk ke tahap selanjutnya";
                }

        }else if(reqPosisi.equalsIgnoreCase("SPV")){
                spv1 = ((umur >= 23 && umur <= 30) && lulusan.equalsIgnoreCase("S1") && pengalaman >= 1);
                spv2 = ((umur >= 25 && umur <= 35) && lulusan.equalsIgnoreCase("D3") && pengalaman >= 4);

                isSpv = spv1 || spv2;

                if(isSpv){
                        result = "Anda akan masuk ke tahap selanjutnya";
                } else {
                        result = "Anda tidak akan masuk ke tahap selanjutnya";
                }
                
        }

        System.out.println(nama + " Terima kasih sudah ikut lowongan");
        System.out.println("Berikut Hasil Lamaran Kerja:");
        System.out.println(reqPosisi);
        System.out.println(result);

    }
}