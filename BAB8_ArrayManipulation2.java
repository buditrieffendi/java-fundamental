import java.util.Scanner;

public class BAB8_ArrayManipulation2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Integer changeNumber, indexTo = 0;
        Integer number[] = {3,2,5,11,7,10,11,3,15,11,17,10};

        System.out.print("Data Array: ");
        System.out.println("{3,2,5,11,7,10,11,3,15,11,17,10}");

        System.out.print("Meruabh nilai angka");
        do{
            System.out.print("Masukan posisi angka 1-13: ");
            indexTo=input.nextInt();
        }while(indexTo<1 || indexTo>13);

        System.out.print("Masukkan angka yang diubah: ");
        changeNumber = input.nextInt();

        for(int i=0; i<number.length;i++){
            if(indexTo-1==i){
                number[i]=changeNumber;
            }
        }

        System.out.println("Data array setelah diubah: ");
        System.out.print("{");
        for(int i=0; i<number.length;i++){
            System.out.print(number[i] + ", ");
        }
        System.out.println("}");
    }
}