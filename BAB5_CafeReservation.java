import java.util.Scanner;

public class BAB5_CafeReservation {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String nama, gender, day, result;
        Integer age;
        Double appearance, money;

        result = "";

        System.out.println("Welcome to cafe 79");
        System.out.print("insert your name:");
        nama = input.nextLine();

        System.out.print("insert your age:");
        age = input.nextInt();

        System.out.print("insert your gender (Female/Male):");
        gender = input.next();

        System.out.print("How good is your appearance(1/10):");
        appearance = input.nextDouble();

        System.out.print("How Much money you have:");
        money = input.nextDouble();

        System.out.print("insert your day(Monday/Tuesday/Wednesday/Thursday/Friday/Saturday/Sunday):");
        day = input.next();

        if(day.equalsIgnoreCase("Monday")) {
            if (age >= 20 && age <= 30 && money >= 500) {
                result = "Reservation successful, you can join us";
            } else {
                result = "Sorry, you cannot join us";
            }
        }else if (day.equalsIgnoreCase("Tuesday")) {
            if (age >= 31 && age <= 50 && money >= 250) {
                result = "Reservation successful, you can join us";
            } else {
                result = "Sorry, you cannot join us";
            }
        }else if (day.equalsIgnoreCase("Wednesday")) {
            if (age >= 20 && age <= 35 && gender.equalsIgnoreCase("Female") && appearance >= 8 && money >= 300) {
                result = "Reservation successful, you can join us";
            } else {
                result = "Sorry, you cannot join us";
            }
        }else if (day.equalsIgnoreCase("Thursday")) {
            if (age >= 21 && age <= 30 && (gender.equalsIgnoreCase("Female") && appearance >= 8 && money >= 300) && (gender.equalsIgnoreCase("Male") && money >= 1000)) {
                result = "Reservation successful, you can join us";
            } else {
                result = "Sorry, you cannot join us";
            }
        }else if (day.equalsIgnoreCase("Friday")) {
            if ((gender.equalsIgnoreCase("Female") && age >= 31 && age <= 45)&&(gender.equalsIgnoreCase("Male") && age >= 21 && age <= 25) && (gender.equalsIgnoreCase("Male")&& appearance >= 8) && (gender.equalsIgnoreCase("Female") && money >= 1000)) {
                result = "Reservation successful, you can join us";
            } else {
                result = "Sorry, you cannot join us";
            }
        }else if (day.equalsIgnoreCase("Saturday")||day.equalsIgnoreCase("Sunday")){ 
            if (age>= 18 && age<= 60 && money >= 100){
                result = "Reservation successful, you can join us";
            }else{
                result = "Sorry, you cannot join us";
            }
        }else{
            result = "Sorry, you cannot join us";
        }

        System.out.println("Hi " + nama + "Thank your for using our application");
        System.out.println("Following result for your reservation ");
        System.out.println(result);
    }
}