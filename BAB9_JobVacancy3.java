import java.util.Scanner;

public class BAB9_JobVacancy3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String nama, jk, lulusan, reqPosisi, hasil = "";
        Integer umur, pengalaman;
        Double penampilan;
        Boolean validate = false;

        System.out.print("Nama : ");
        nama = input.nextLine();
        System.out.print("Umur : ");
        umur = input.nextInt();
        System.out.print("Jenis Kelamin(Pria/Wanita) : ");
        jk = input.next();
        System.out.print("Lulusan : ");
        lulusan = input.next();
        System.out.print("Pengalaman (Tahun) : ");
        pengalaman = input.nextInt();
        System.out.print("Penampilan (1/10) : ");
        penampilan = input.nextDouble();
        System.out.println("Posisi yang akan dilamar (Koordinator, Admin, SPV):");
        reqPosisi = input.next();

        if(reqPosisi.equalsIgnoreCase("Koordinator")){
            validate = reqKoor(umur, jk, lulusan, pengalaman, penampilan);

        }else if(reqPosisi.equalsIgnoreCase("Admin")){
            validate = reqAdmin(umur, jk, lulusan, pengalaman, penampilan);

        }else if(reqPosisi.equalsIgnoreCase("SPV")){
            validate = reqSpv(umur, jk, lulusan, pengalaman, penampilan);
        }

        if(validate){
            hasil = "Anda akan masuk ke tahap selanjutnya";
        } else {
            hasil = "Anda tidak akan masuk ke tahap selanjutnya";
        }

        printResult(nama, hasil, reqPosisi);
    }

    public static Boolean reqKoor(int umur,String jk,String lulusan,int pengalaman,Double penampilan){
        Boolean isKoor, koor1, koor2, result;

        koor1 = (jk.equalsIgnoreCase("Pria") && umur >= 21 && umur <= 30 && lulusan.equalsIgnoreCase("SMK") || lulusan.equalsIgnoreCase("D3") && pengalaman >= 2);
        koor2 = (jk.equalsIgnoreCase("Pria") && umur >= 30 && lulusan.equalsIgnoreCase("S1") && pengalaman >= 5);

        isKoor = koor1 || koor2;

        if(isKoor){
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    public static Boolean reqAdmin(int umur,String jk,String lulusan,int pengalaman,Double penampilan){
        Boolean isAdmin, admin1, admin2, admin3, result;

        admin1 = (jk.equalsIgnoreCase("Wanita") && (umur >= 20 && umur <= 25) && lulusan.equalsIgnoreCase("D3") && (penampilan >= 8.5 || pengalaman >= 1));
        admin2 = (jk.equalsIgnoreCase("Wanita") && umur >= 25 && lulusan.equalsIgnoreCase("S1") && penampilan >= 8.5 && pengalaman >= 3);
        admin3 = (jk.equalsIgnoreCase("Pria") && (umur >= 20 && umur <= 30) && penampilan >= 8.5 && (lulusan.equalsIgnoreCase("D3") || lulusan.equalsIgnoreCase("S1")) && pengalaman >= 2);

        isAdmin = admin1 || admin2 || admin3;

        if(isAdmin){
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    public static Boolean reqSpv(int umur,String jk,String lulusan,int pengalaman,Double penampilan){
        Boolean isSpv, spv1, spv2, result;

        spv1 = ((umur >= 23 && umur <= 30) && lulusan.equalsIgnoreCase("S1") && pengalaman >= 1);
        spv2 = ((umur >= 25 && umur <= 35) && lulusan.equalsIgnoreCase("D3") && pengalaman >= 4);

        isSpv = spv1 || spv2;

        if(isSpv){
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    public static void printResult(String nama, String hasil, String reqPosisi){
        System.out.println(nama + ", Terima kasih sudah mengikuti lowongan kerja");
        System.out.println("Berikut Hasil Lamaran Kerja:");
        System.out.println(reqPosisi);
        System.out.println(hasil);
    }

}