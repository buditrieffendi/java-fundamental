import java.util.Scanner;

public class BAB4_JobVacancy {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String nama, jk, lulusan;
        Integer umur, pengalaman;
        Double penampilan;
        Boolean isKoor, koor1, koor2, isAdmin, admin1, admin2;

        System.out.print("Nama : ");
        nama = input.nextLine();
        System.out.print("Umur : ");
        umur = input.nextInt();
        System.out.print("Jenis Kelamin(Pria/Wanita) : ");
        jk = input.next();
        System.out.print("Lulusan : ");
        lulusan = input.next();
        System.out.print("Pengalaman (Tahun) : ");
        pengalaman = input.nextInt();
        System.out.print("Penampilan (1/10) : ");
        penampilan = input.nextDouble();

        koor1 = (jk.equalsIgnoreCase("Pria") && umur >= 21 && umur <= 30 && lulusan.equalsIgnoreCase("SMK")
                || lulusan.equalsIgnoreCase("D3") && pengalaman >= 2);
        koor2 = (jk.equalsIgnoreCase("Pria") && umur >= 30 && lulusan.equalsIgnoreCase("S1") && pengalaman >= 2);
        admin1 = (jk.equalsIgnoreCase("Wanita") && (umur >= 20 && umur <= 25) && lulusan.equalsIgnoreCase("D3")
                && (penampilan >= 8.5 || pengalaman >= 1));
        admin2 = (jk.equalsIgnoreCase("Wanita") && umur >= 25 && lulusan.equalsIgnoreCase("S1") && penampilan >= 8.5
                && pengalaman >= 5);

        isKoor = koor1 || koor2;
        isAdmin = admin1 || admin2;

        System.out.println(nama + " Terima kasih sudah ikut lowongan");
        System.out.println();
        System.out.println("Hasil kelulusan lowongan Koordinator: " + isKoor);
        System.out.println("Hasil kelulusan lowongan Admin: " + isAdmin);

    }
}